﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LinqTests
{
    class Person
    {
        private string _name;
        private string _lastname;
        private int _salary;
        private PersonAddress _address;

        public Person(string name, string lastname, int salary, PersonAddress address)
        {
            _name = name;
            _lastname = lastname;
            _address = address;
            _salary = salary;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Lastname
        {
            get { return _lastname; }
            set { _lastname = value; }
        }

        public PersonAddress Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public int Salary
        {
            get { return _salary; }
            set { _salary = value; }
        }
    }

    public class PersonAddress
    {
        private string _street;
        private string _city;
        private string _postal;

        public PersonAddress(string street, string city, string postal)
        {
            _street = street;
            _city = city;
            _postal = postal;
        }

        public string Street
        {
            get { return _street; }
            set { _street = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string Postal
        {
            get { return _postal; }
            set { _postal = value; }
        }
    }
}
