﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LinqTests
{
    [TestClass]
    public class UnitTest1
    {
        private List<Person> _list;
        

        [TestMethod]
        public void StreetsOnly()
        {
            var streets = LinqAnswers.GetStreets(_list);
            Assert.AreEqual(9, streets.Count);
            Assert.IsTrue(streets.Contains("Wisniowa"));
        }
        [DataTestMethod]
        [DataRow('K', 0)]
        [DataRow('A', 4)]
        [DataRow('T', 2)]
        [DataRow('Y', 0)]
        public void NumberOfPersonsWithNameStartingWithLetter(char letter, int result)
        {
            var numberOfOccurences = LinqAnswers.CountPersonWithNameStartingWithLeter(_list, letter);
            Assert.AreEqual(result, numberOfOccurences);
        }

        public void MinimalSalary()
        {
            var minimalSalary = LinqAnswers.MinimalSalary(_list);
            Assert.AreEqual(1002, minimalSalary);
        }
        [DataTestMethod]
        [DataRow("Warszawa", 2)]
        [DataRow("Krakow", 1)]
        [DataRow("Szczecin", 1)]
        [DataRow("Radom", 0)]
        public void NumberOfPersonsFromCity(string city, int result)
        {
            var numberOfPersons = LinqAnswers.NumberOfPersonFromCity(_list, city);
            Assert.AreEqual(result, numberOfPersons);
        }
        [TestMethod]
        public void LastNamesWithSalaries()
        {
            var tuples = LinqAnswers.GetLastNamesWithSalaries(_list);
            Assert.AreEqual(tuples.Count, 9);
            Assert.IsTrue(tuples.Contains(new Tuple<string, int>("Opacki", 51251)));
            Assert.IsTrue(tuples.Contains(new Tuple<string, int>("Abacki", 1612)));
        }
        [TestInitialize()]
        public void Startup()
        {
            _list = new List<Person>
            {
                new Person("Adam", "Abacki", 1612, new PersonAddress("Pulawska", "Warszawa", "01-111")),
                new Person("Jan", "Babacki", 2001, new PersonAddress("Kazimierzowska", "Krakow", "02-223")),
                new Person("Tomasz", "Cacacki", 5006, new PersonAddress("Wisniowa", "Poznan", "03-323")),
                new Person("Tomasz", "Dadacki", 2051, new PersonAddress("Arbuzowa", "Szczecin", "04-423")),
                new Person("Artur", "Fafacki", 5121, new PersonAddress("Aluzyjna", "Warszawa", "05-523")),
                new Person("Andrzej", "Tatacki", 10005, new PersonAddress("Pruszkowska", "Poznan", "06-623")),
                new Person("Iwan", "Opacki", 51251, new PersonAddress("Warszawska", "Zgierz", "07-723")),
                new Person("Denis", "Kakacki", 1005, new PersonAddress("Mandarynki", "Torun", "08-823")),
                new Person("Artur", "Zazacki", 1002, new PersonAddress("Mandarynki", "Berlin", "09-923")),
                };

        }
    }
}
